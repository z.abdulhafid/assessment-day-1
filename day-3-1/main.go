package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var angka = []int{23, 45, 67, 54, 66, 19, 56, 78, 89, 44, 11, 22, 33, 44, 55, 66, 77, 88, 99, 23, 34, 32, 23, 12}

	group := divideToGroup(angka)
	fmt.Println("Group lama adalah:", group)

	var arrayOfGroup [3]Group

	for i, array := range group {
		arrayOfGroup[i].Arr = array
	}

	fmt.Println("Array of Group adalah:")
	fmt.Println(arrayOfGroup)

	for i := 0; i < 3; i++ {
		arrayOfGroup[i].divideTheGroup()
	}
	fmt.Println("Group baru adalah:", arrayOfGroup)

	findResult(&arrayOfGroup[0], &arrayOfGroup[1], &arrayOfGroup[2])

	maxGroup, minGroup := findMaxMinGroup(arrayOfGroup[0], arrayOfGroup[1], arrayOfGroup[2])

	fmt.Println("Group 1:", arrayOfGroup[0].getArr())
	fmt.Println("Group 2:", arrayOfGroup[1].getArr())
	fmt.Println("Group 3:", arrayOfGroup[2].getArr())

	fmt.Println("Total 1:", arrayOfGroup[0].getTotal())
	fmt.Println("Total 2:", arrayOfGroup[1].getTotal())
	fmt.Println("Total 3:", arrayOfGroup[2].getTotal())

	fmt.Println("Nilai max kumpulan 1", arrayOfGroup[0].getMax())
	fmt.Println("Nilai min kumpulan 1", arrayOfGroup[0].getMin())

	fmt.Println("Nilai max kumpulan 2", arrayOfGroup[1].getMax())
	fmt.Println("Nilai min kumpulan 2", arrayOfGroup[1].getMin())

	fmt.Println("Nilai max kumpulan 3", arrayOfGroup[2].getMax())
	fmt.Println("Nilai min kumpulan 3", arrayOfGroup[2].getMin())

	fmt.Println("Rata-rata 1:", arrayOfGroup[0].getAvg())
	fmt.Println("Rata-rata 2:", arrayOfGroup[1].getAvg())
	fmt.Println("Rata-rata 3:", arrayOfGroup[2].getAvg())

	fmt.Println("Grop terbesar adalah", maxGroup.getArr())
	fmt.Println("Grop terkecil adalah", minGroup.getArr())

}

func errLogs() {
	message := recover()
	if message != nil {
		fmt.Println("Error:", message)
	}
}

func divider(index int, value int, divisor int) (int, error) {
	defer errLogs()

	if divisor == 0 {
		return 100, errors.New("Pembagi di index " + fmt.Sprint(index) + " Adalah 0")
	}
	result := value / divisor
	return result, nil
}

func divideToGroup(angka []int) [3][]int {
	length := len(angka) / 3
	var group [3][]int

	for i := 0; i < 3; i++ {
		group[i] = angka[length*i : length*(i+1)]
	}

	return group
}

func (group *Group) divideTheGroup() {
	rand.Seed(time.Now().UnixNano())
	fmt.Println("Fungsi Pembagi untuk group dijalankan")
	for index, value := range group.Arr {
		var err error
		randomNum := rand.Intn(3)
		group.Arr[index], err = divider(index, value, randomNum)
		if err != nil {
			fmt.Println(err)
		}
	}
	fmt.Println("-----------")
}

func findResult(group1 *Group, group2 *Group, group3 *Group) {
	group1.Max = group1.Arr[0]
	group1.Min = group1.Arr[0]
	group2.Max = group2.Arr[0]
	group2.Min = group2.Arr[0]
	group3.Max = group3.Arr[0]
	group3.Min = group3.Arr[0]

	for i := 0; i < len(group1.Arr); i++ {
		//cari min max total group 1
		if group1.Max < group1.Arr[i] {
			group1.Max = group1.Arr[i]
		}

		if group1.Min > group1.Arr[i] {
			group1.Min = group1.Arr[i]
		}

		group1.Total += group1.Arr[i]
	}

	for i := 0; i < len(group2.Arr); i++ {
		//cari min max total group 2
		if group2.Max < group2.Arr[i] {
			group2.Max = group2.Arr[i]
		}

		if group2.Min > group2.Arr[i] {
			group2.Min = group2.Arr[i]
		}

		group2.Total += group2.Arr[i]
	}

	for i := 0; i < len(group3.Arr); i++ {
		//cari min max total group 3
		if group3.Max < group3.Arr[i] {
			group3.Max = group3.Arr[i]
		}

		if group3.Min > group3.Arr[i] {
			group3.Min = group3.Arr[i]
		}

		group3.Total += group3.Arr[i]
	}

	group1.Avg = float32(group1.Total / len(group1.Arr))
	group2.Avg = float32(group2.Total / len(group2.Arr))
	group3.Avg = float32(group3.Total / len(group3.Arr))
}

func findMaxMinGroup(group1 ThisGroup, group2 ThisGroup, group3 ThisGroup) (ThisGroup, ThisGroup) {
	group := [3]ThisGroup{group1, group2, group3}

	var maxGroup int = 0
	var minGroup int = 0
	for i := 0; i < 2; i++ {
		if group[i].getMax() > group[i+1].getMax() {
			maxGroup = i
		}

		if group[i].getMin() < group[i+1].getMin() {
			minGroup = i
		}
	}

	return group[maxGroup], group[minGroup]
}

// Group comment.
type Group struct {
	Min, Max, Total int
	Avg             float32
	Arr             []int
}

// ThisGroup comment.
type ThisGroup interface {
	getMin() int
	getMax() int
	getTotal() int
	getAvg() float32
	getArr() []int
}

func (group Group) getMin() int {
	return group.Min
}

func (group Group) getMax() int {
	return group.Max
}

func (group Group) getTotal() int {
	return group.Total
}

func (group Group) getAvg() float32 {
	return group.Avg
}

func (group Group) getArr() []int {
	return group.Arr
}
