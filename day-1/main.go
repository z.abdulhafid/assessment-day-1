package main

import "fmt"

func main() {
	var angka = [...]int{23, 45, 67, 54, 66, 19, 56, 78, 89, 44, 11, 22, 33, 44, 55, 66, 77, 88, 99, 23, 34, 32, 23, 12}

	var group1 = angka[0 : len(angka)/3]
	var group2 = angka[len(angka)/3 : (len(angka)/3)*2]
	var group3 = angka[(len(angka)/3)*2 : (len(angka)/3)*3]

	fmt.Println("Grup 1:", group1)
	fmt.Println("Grup 2:", group2)
	fmt.Println("Grup 3:", group3)

	var total = [3]int{0, 0, 0}

	var capsMax = [3]int{group1[0], group2[0], group3[0]}
	var capsMin = [3]int{group1[0], group2[0], group3[0]}

	for i := 0; i < len(group1); i++ {
		if capsMax[0] < group1[i] {
			capsMax[0] = group1[i]
		}

		if capsMin[0] > group1[i] {
			capsMin[0] = group1[i]
		}

		total[0] += group1[i]
	}

	for i := 0; i < len(group2); i++ {
		if capsMax[1] < group2[i] {
			capsMax[1] = group2[i]
		}

		if capsMin[1] > group2[i] {
			capsMin[1] = group2[i]
		}

		total[1] += group2[i]
	}

	for i := 0; i < len(group3); i++ {
		if capsMax[2] < group3[i] {
			capsMax[2] = group3[i]
		}

		if capsMin[2] > group3[i] {
			capsMin[2] = group3[i]
		}

		total[2] += group3[i]
	}

	fmt.Println("Total 1:", total[0])
	fmt.Println("Total 2:", total[1])
	fmt.Println("Total 3:", total[2])

	var mean1 float32 = float32(total[0] / len(group1))
	var mean2 float32 = float32(total[1] / len(group2))
	var mean3 float32 = float32(total[2] / len(group3))

	fmt.Println("Rata-rata 1:", mean1)
	fmt.Println("Rata-rata 2:", mean2)
	fmt.Println("Rata-rata 3:", mean3)

	for i := 0; i < 3; i++ {
		fmt.Println("Nilai max kumpulan", i+1, capsMax[i])
		fmt.Println("Nilai min kumpulan", i+1, capsMin[i])
	}

	maxGroup := 1
	minGroup := 1
	for i := 0; i < 2; i++ {
		if total[i] > total[i+1] {
			maxGroup = i
		}

		if total[i] < total[i+1] {
			minGroup = i
		}
	}

	switch maxGroup {
	case 0:
		fmt.Println("Total group terbesar adalah", group1)
	case 1:
		fmt.Println("Total group terbesar adalah", group2)
	case 2:
		fmt.Println("Total group terbesar adalah", group3)
	default:
		fmt.Println("Tidak ditemukan group terbesar")
	}

	switch minGroup {
	case 0:
		fmt.Println("Total group terkecil adalah", group1)
	case 1:
		fmt.Println("Total group terkecil adalah", group2)
	case 2:
		fmt.Println("Total group terkecil adalah", group3)
	default:
		fmt.Println("Tidak ditemukan group terkecil")
	}
}
