package main

import "fmt"

func main() {
	var angka = []int{23, 45, 67, 54, 66, 19, 56, 78, 89, 44, 11, 22, 33, 44, 55, 66, 77, 88, 99, 23, 34, 32, 23, 12}
	group := divideToGroup(angka)

	total, min, max, avg := findResult(group)

	maxIndex, minIndex := findMaxMinGroup(total)

	fmt.Println("Group 1:", group[0])
	fmt.Println("Group 2:", group[1])
	fmt.Println("Group 3:", group[2])

	fmt.Println("Total 1:", total[0])
	fmt.Println("Total 2:", total[1])
	fmt.Println("Total 3:", total[2])

	fmt.Println("Nilai max kumpulan 1", max[0])
	fmt.Println("Nilai min kumpulan 1", min[0])

	fmt.Println("Nilai max kumpulan 2", max[1])
	fmt.Println("Nilai min kumpulan 2", min[1])

	fmt.Println("Nilai max kumpulan 3", max[2])
	fmt.Println("Nilai min kumpulan 3", min[2])

	fmt.Println("Rata-rata 1:", avg[0])
	fmt.Println("Rata-rata 2:", avg[1])
	fmt.Println("Rata-rata 3:", avg[2])

	fmt.Println("Grop terbesar adalah", group[maxIndex])
	fmt.Println("Grop terkecil adalah", group[minIndex])

}

func divideToGroup(angka []int) [3][]int {
	length := len(angka) / 3
	var group [3][]int

	for i := 0; i < 3; i++ {
		group[i] = angka[length*i : length*(i+1)]
	}

	return group
}

func findMaxMinGroup(total [3]int) (int, int) {
	maxGroup := 1
	minGroup := 1
	for i := 0; i < 2; i++ {
		if total[i] > total[i+1] {
			maxGroup = i
		}

		if total[i] < total[i+1] {
			minGroup = i
		}
	}

	return maxGroup, minGroup
}

func findResult(group [3][]int) ([3]int, [3]int, [3]int, [3]float32) {
	var total [3]int
	var min [3]int
	var max [3]int
	var avg [3]float32

	min[0] = group[0][0]
	max[0] = group[0][0]
	min[1] = group[1][0]
	max[1] = group[1][0]
	min[2] = group[2][0]
	max[2] = group[2][0]
	for i := 0; i < len(group[0]); i++ {

		//cari min max total group 1
		if max[0] < group[0][i] {
			max[0] = group[0][i]
		}

		if min[0] > group[0][i] {
			min[0] = group[0][i]
		}

		total[0] += group[0][i]

		//cari min max total group 2
		if max[1] < group[1][i] {
			max[1] = group[1][i]
		}

		if min[1] > group[1][i] {
			min[1] = group[1][i]
		}

		total[1] += group[1][i]

		//cari min max total group 3
		if max[2] < group[2][i] {
			max[2] = group[2][i]
		}

		if min[2] > group[2][i] {
			min[2] = group[2][i]
		}

		total[2] += group[2][i]
	}

	avg[0] = float32(total[0] / len(group[0]))
	avg[1] = float32(total[1] / len(group[1]))
	avg[2] = float32(total[2] / len(group[2]))

	return total, min, max, avg
}
