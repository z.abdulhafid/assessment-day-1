package main

import (
	"assesmentGoRESTAPI/controller"
	"strconv"

	"github.com/gin-gonic/gin"
)

func main() {
	gin.SetMode(gin.ReleaseMode)

	router := gin.Default()

	router.POST("/tambah", func(c *gin.Context) {
		stringAngka := c.PostFormArray("numbers")
		angka := toInt(stringAngka)

		c.JSON(200, gin.H{
			"hasil_kalkulasi": controller.Tambah(angka),
		})
	})

	router.POST("/kurang", func(c *gin.Context) {
		stringAngka := c.PostFormArray("numbers")
		angka := toInt(stringAngka)

		c.JSON(200, gin.H{
			"hasil_kalkulasi": controller.Kurang(angka),
		})
	})

	router.POST("/kali", func(c *gin.Context) {
		stringAngka := c.PostFormArray("numbers")
		angka := toInt(stringAngka)

		c.JSON(200, gin.H{
			"hasil_kalkulasi": controller.Kali(angka, len(angka)-1),
		})
	})

	router.POST("/bagi", func(c *gin.Context) {
		stringAngka := c.PostFormArray("numbers")
		angka := toInt(stringAngka)

		c.JSON(200, gin.H{
			"hasil_kalkulasi": controller.Bagi(angka, len(angka)-1),
		})
	})

	router.POST("/faktorial", func(c *gin.Context) {
		stringAngka := c.PostForm("numbers")
		hasil, _ := strconv.Atoi(stringAngka)

		c.JSON(200, gin.H{
			"hasil_kalkulasi": controller.Faktorial(hasil),
		})
	})

	router.Run()
}

func toInt(stringAngka []string) []int {
	var angka []int
	for _, strAngka := range stringAngka {
		something, _ := strconv.Atoi(strAngka)
		angka = append(angka, something)
	}
	return angka
}
