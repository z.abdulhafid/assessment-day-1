package controller

// Tambah comment.
func Tambah(values []int) int {
	var total int = 0
	for _, num := range values {
		total += num
	}
	return total
}

// Kurang comment.
func Kurang(values []int) int {
	var total int = 0
	for _, num := range values {
		total -= num
	}
	return total
}

// Kali comment.
func Kali(values []int, index int) int {
	if index < 0 {
		return 1
	}
	return values[index] * Kali(values, index-1)
}

// Bagi comment.
func Bagi(values []int, index int) float32 {
	result := float32(values[0])
	for i := 1; i < len(values); i++ {
		result /= float32(values[i])
	}
	return result
}

// Faktorial comment.
func Faktorial(value int) int {
	if value <= 1 {
		return 1
	}

	return value * Faktorial(value-1)
}
