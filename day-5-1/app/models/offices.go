package models

import (
	"gorm.io/gorm"
)

// Office comment.
type Office struct {
	gorm.Model
	Name    string
	Address string
}
