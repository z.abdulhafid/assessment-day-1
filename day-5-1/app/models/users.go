package models

import (
	"gorm.io/gorm"
)

//User comment.
type User struct {
	gorm.Model
	Fullname string
	Email    string
	Password string
	OfficeID int
	Office   Office `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL"`
}
