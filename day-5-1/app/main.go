package main

import (
	"connection/app/config"
	"connection/app/controllers"

	"github.com/gin-gonic/gin"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	// database connection
	db := config.Connect()
	strDB := controllers.StrDB{DB: db}

	// migration
	// models.Migrations(db)

	router := gin.Default()

	office := router.Group("office")
	{
		office.GET("/", strDB.GetOffice)
		office.GET("/show/:id", strDB.GetOneOffice)
		office.GET("/search", strDB.GetSearchOffice)
		office.POST("/", strDB.CreateOffice)
		office.PUT("/:id", strDB.UpdateOffice)
		office.DELETE("/:id", strDB.DeleteOffice)
	}

	user := router.Group("user")
	{
		user.GET("/", strDB.GetUser)
		user.GET("/show/:id", strDB.GetOneUser)
		user.GET("/search", strDB.GetSearchUser)
		user.POST("/", strDB.CreateUser)
		user.PUT("/:id", strDB.UpdateUser)
		user.DELETE("/:id", strDB.DeleteUser)
	}

	todos := router.Group("todos")
	{
		todos.GET("/", strDB.GetTodos)
		todos.GET("/show/:id", strDB.GetTodos)
		todos.GET("/search", strDB.GetTodos)
		todos.POST("/", strDB.CreateTodos)
		todos.PUT("/:id", strDB.UpdateTodos)
		todos.DELETE("/:id", strDB.DeleteTodos)
	}

	router.Run()
}
