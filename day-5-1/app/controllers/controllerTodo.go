package controllers

import (
	"connection/app/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateTodos comment.
func (strDB *StrDB) CreateTodos(c *gin.Context) {
	var (
		todos  models.Todos
		result gin.H
	)

	err := c.Bind(&todos)
	if err != nil {
		fmt.Println("data not found")
	}
	err = strDB.DB.Create(&todos).Error
	if err != nil {
		result = gin.H{
			"message": "resource not created",
		}
	} else {
		result = gin.H{
			"message": "resource created successfully",
			"result":  todos,
		}
	}
	c.JSON(http.StatusOK, result)
}

// UpdateTodos comment.
func (strDB *StrDB) UpdateTodos(c *gin.Context) {
	var (
		todos    models.Todos
		newTodos models.Todos
		result   gin.H
	)

	id := c.Param("id")
	err := c.Bind(&newTodos)

	err = strDB.DB.First(&todos, id).Error
	if err != nil {
		result = gin.H{
			"message": "data not found",
		}
	}

	err = strDB.DB.Model(&todos).Updates(newTodos).Error
	if err != nil {
		result = gin.H{
			"message": "update failed",
		}
	} else {
		result = gin.H{
			"message": "resource updated successfully",
			"result":  todos,
		}
	}

	c.JSON(http.StatusOK, result)
}

//DeleteTodos comment.
func (strDB *StrDB) DeleteTodos(c *gin.Context) {
	var (
		todos  models.Todos
		result gin.H
	)

	id := c.Param("id")
	err := strDB.DB.First(&todos, id).Error
	if err != nil {
		result = gin.H{
			"message": "data not found!",
		}
	}

	err = strDB.DB.Delete(&todos).Error
	if err != nil {
		result = gin.H{
			"message": "deleting resource failed",
		}
	} else {
		result = gin.H{
			"message": "data deleted successfully",
		}
	}
	c.JSON(http.StatusOK, result)
}

// GetTodos comment.
func (strDB *StrDB) GetTodos(c *gin.Context) {
	var (
		todos  []models.Todos
		result gin.H
	)

	strDB.DB.Find(&todos)

	if len(todos) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = responseAPI(todos, len(todos))
	}

	c.JSON(http.StatusOK, result)
}

// GetOneTodos comment.
func (strDB *StrDB) GetOneTodos(c *gin.Context) {
	var (
		todos  models.Todos
		result gin.H
	)

	id := c.Param("id")
	err := strDB.DB.First(&todos, id).Error
	if err != nil {
		result = gin.H{
			"message": "resource not found",
		}
	} else {
		result = gin.H{
			"message": "resource found",
			"result":  todos,
		}
	}

	c.JSON(http.StatusOK, result)
}

// GetSearchTodos comment.
func (strDB *StrDB) GetSearchTodos(c *gin.Context) {
	var (
		todos  []models.Todos
		result gin.H
	)

	name := c.DefaultQuery("name", "")
	description := c.DefaultQuery("description", "")
	userID := c.DefaultQuery("userid", "0")
	err := strDB.DB.Find(&todos, "name = ? and description = ? and user_id = ?", name, description, userID).Error
	if err != nil {
		result = gin.H{
			"message": "resource not found",
		}
	} else {
		result = responseAPI(todos, len(todos))
	}

	c.JSON(http.StatusOK, result)
}
