package controllers

import (
	"connection/app/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CreateUser comment.
func (strDB *StrDB) CreateUser(c *gin.Context) {
	var (
		user   models.User
		result gin.H
	)

	err := c.Bind(&user)
	if err != nil {
		fmt.Println("data not found")
	}
	err = strDB.DB.Create(&user).Error
	if err != nil {
		result = gin.H{
			"message": "resource not created",
		}
	} else {
		result = gin.H{
			"message": "resource created successfully",
			"result":  user,
		}
	}
	c.JSON(http.StatusOK, result)
}

// UpdateUser comment.
func (strDB *StrDB) UpdateUser(c *gin.Context) {
	var (
		user    models.User
		newUser models.User
		result  gin.H
	)

	id := c.Param("id")
	err := c.Bind(&newUser)

	err = strDB.DB.First(&user, id).Error
	if err != nil {
		result = gin.H{
			"message": "data not found",
		}
	}

	err = strDB.DB.Model(&user).Updates(newUser).Error
	if err != nil {
		result = gin.H{
			"message": "update failed",
		}
	} else {
		result = gin.H{
			"message": "resource updated successfully",
			"result":  user,
		}
	}

	c.JSON(http.StatusOK, result)
}

//DeleteUser comment.
func (strDB *StrDB) DeleteUser(c *gin.Context) {
	var (
		user   models.User
		result gin.H
	)

	id := c.Param("id")
	err := strDB.DB.First(&user, id).Error
	if err != nil {
		result = gin.H{
			"message": "data not found!",
		}
	}
	// fmt.Println(user)

	err = strDB.DB.Delete(&user).Error
	if err != nil {
		result = gin.H{
			"message": "deleting resource failed",
		}
	} else {
		result = gin.H{
			"message": "data deleted successfully",
		}
	}
	c.JSON(http.StatusOK, result)
}

// GetUser comment.
func (strDB *StrDB) GetUser(c *gin.Context) {
	var (
		user   []models.User
		result gin.H
	)

	strDB.DB.Find(&user)

	if len(user) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = responseAPI(user, len(user))
	}

	c.JSON(http.StatusOK, result)
}

// GetOneUser comment.
func (strDB *StrDB) GetOneUser(c *gin.Context) {
	var (
		user   models.User
		result gin.H
	)

	id := c.Param("id")
	err := strDB.DB.First(&user, id).Error
	if err != nil {
		result = gin.H{
			"message": "resource not found",
		}
	} else {
		result = gin.H{
			"message": "resource found",
			"result":  user,
		}
	}

	c.JSON(http.StatusOK, result)
}

// GetSearchUser comment.
func (strDB *StrDB) GetSearchUser(c *gin.Context) {
	var (
		user   []models.User
		result gin.H
	)

	fullName := c.DefaultQuery("fullname", "")
	email := c.DefaultQuery("email", "")
	officeID := c.DefaultQuery("officeid", "0")
	err := strDB.DB.Find(&user, "fullname = ? and email = ? and office_id = ?", fullName, email, officeID).Error
	if err != nil {
		result = gin.H{
			"message": "resource not found",
		}
	} else {
		result = responseAPI(user, len(user))
	}

	c.JSON(http.StatusOK, result)
}
