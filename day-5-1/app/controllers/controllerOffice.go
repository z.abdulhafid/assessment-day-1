package controllers

import (
	"connection/app/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

//CreateOffice comment.
func (strDB *StrDB) CreateOffice(c *gin.Context) {
	var (
		office models.Office
		result gin.H
	)

	err := c.Bind(&office)
	if err != nil {
		fmt.Println("tidak ada data")
	}
	strDB.DB.Create(&office)
	result = gin.H{
		"result": office,
	}
	c.JSON(http.StatusOK, result)
}

//UpdateOffice comment.
func (strDB *StrDB) UpdateOffice(c *gin.Context) {
	var (
		office    models.Office
		newOffice models.Office
		result    gin.H
	)

	id := c.Param("id")
	err := c.Bind(&newOffice)

	err = strDB.DB.First(&office, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}

	err = strDB.DB.Model(&office).Updates(newOffice).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "resource updated successfully",
		}
	}

	c.JSON(http.StatusOK, result)
}

//DeleteOffice comment.
func (strDB *StrDB) DeleteOffice(c *gin.Context) {
	var (
		office models.Office
		result gin.H
	)

	id := c.Param("id")
	err := strDB.DB.First(&office, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found!",
		}
	}
	// fmt.Println(office)

	err = strDB.DB.Delete(&office).Error
	if err != nil {
		result = gin.H{
			"result": "data failed",
		}
	} else {
		result = gin.H{
			"result": "data deleted successfully",
		}
	}
	c.JSON(http.StatusOK, result)
}

// GetOffice comment.
func (strDB *StrDB) GetOffice(c *gin.Context) {
	var (
		office []models.Office
		result gin.H
	)

	strDB.DB.Find(&office)

	if len(office) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = responseAPI(office, len(office))
	}

	c.JSON(http.StatusOK, result)
}

// GetOneOffice comment.
func (strDB *StrDB) GetOneOffice(c *gin.Context) {
	var (
		office models.Office
		result gin.H
	)

	id := c.Param("id")
	err := strDB.DB.First(&office, id).Error
	if err != nil {
		result = gin.H{
			"result": "resource not found",
		}
	} else {
		result = gin.H{
			"result": office,
		}
	}

	c.JSON(http.StatusOK, result)
}

// GetSearchOffice comment.
func (strDB *StrDB) GetSearchOffice(c *gin.Context) {
	var (
		office []models.Office
		result gin.H
	)

	name := c.DefaultQuery("name", "")
	address := c.DefaultQuery("address", "")
	err := strDB.DB.Find(&office, "name = ? and address = ?", name, address).Error
	if err != nil {
		result = gin.H{
			"result": "resource not found",
		}
	} else {
		result = responseAPI(office, len(office))
	}

	c.JSON(http.StatusOK, result)
}
