package main

import (
	"email/app/config"
	"email/app/controllers"
	"email/app/models"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jasonlvhit/gocron"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	db := config.Connect()
	strDB := controllers.StrDB{DB: db}
	// migration
	models.Migrations(db)

	router := gin.Default()

	// strDB.CreateEmail()

	runCron(&strDB)

	router.Run()
	// sendEmail()
}

func runCron(strDB *controllers.StrDB) {
	// gocron.Every(1).Second().Do(task)
	// gocron.Every(2).Second().Do(taskWithParams, 1, "hello")
	gocron.Every(15).Minute().Do(strDB.SentEmail)
	<-gocron.Start()
}

func task() {
	fmt.Println("I am running task.")
}

func taskWithParams(a int, b string) {
	fmt.Println(a, b)
}
