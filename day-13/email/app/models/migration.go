package models

import (
	"fmt"

	"gorm.io/gorm"
)

// Migrations comment.
func Migrations(db *gorm.DB) {
	var check bool
	check = db.Migrator().HasTable(&Email{})
	if !check {
		db.Migrator().CreateTable(&Email{})
		fmt.Println("Create Table Email")
	}
}
