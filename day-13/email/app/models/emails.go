package models

import (
	"time"

	"gorm.io/gorm"
)

// Email struct
type Email struct {
	gorm.Model
	To      string
	Subject string
	Message string
	SentAt  time.Time
	IsSent  bool
}
