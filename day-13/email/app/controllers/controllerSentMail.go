package controllers

import (
	"email/app/models"
	"fmt"
	"log"
	"net/smtp"
	"os"
	"strings"
	"time"
)

const (
	// See http://golang.org/pkg/time/#Parse
	timeFormat = "2006-01-02 15:04 UTC+7"
)

// CreateEmail function
func (queryDB *StrDB) CreateEmail() {
	var email models.Email

	responder := []string{"jackbenny93@gmail.com", "nesago5893@septicvernon.com"}
	t, _ := time.Parse(time.RFC3339, "2020-10-14T16:00:00+07:00")

	for _, resp := range responder {
		email = models.Email{
			To:      resp,
			Subject: "Ini test email",
			Message: "Ini test email",
			SentAt:  t,
		}

		queryDB.DB.Create(email)
	}
}

//SentEmail function
func (queryDB *StrDB) SentEmail() {
	var (
		emails []models.Email
	)
	now := time.Now()
	then := now.Add(time.Minute * -15)

	err := queryDB.DB.Where("is_sent = ? AND sent_at BETWEEN ? AND ?", 0, then, now).Find(&emails).Error
	if err != nil {
		fmt.Println("Tidak ada email yang bisa dikirim")
	} else {
		for _, email := range emails {
			sendEmail(&email, queryDB)
		}
	}
	fmt.Println("fungsi email jalan")
}

func sendEmail(email *models.Email, queryDB *StrDB) {
	to := []string{email.To}
	cc := []string{"hafidzainul@soizee.com"}

	subject := email.Subject
	message := email.Message

	err := sendMail(to, cc, subject, message)

	if err != nil {
		log.Fatal(err.Error())
	}

	log.Println("Mail sent")
	email.IsSent = false
	queryDB.DB.Save(&email)
}

func sendMail(to []string, cc []string, subject, message string) error {
	body := "From: " + os.Getenv("CONFIG_EMAIL") + "\n" +
		"To: " + strings.Join(to, ",") + "\n" +
		"Cc: " + strings.Join(cc, ",") + "\n" +
		"Subject: " + subject + "\n\n" +
		message

	auth := smtp.PlainAuth("", os.Getenv("CONFIG_EMAIL"), os.Getenv("CONFIG_PASSWORD"), os.Getenv("CONFIG_SMTP_HOST"))

	smtpAddr := os.Getenv("CONFIG_SMTP_HOST") + ":" + os.Getenv("CONFIG_SMTP_PORT")

	err := smtp.SendMail(smtpAddr, auth, os.Getenv("CONFIG_EMAIL"), append(to, cc...), []byte(body))

	if err != nil {
		return err
	}

	return nil
}
