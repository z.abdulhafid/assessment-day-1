package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
)

func main() {
	apis := []map[string]string{
		{
			"source": "google",
			"key":    "AIzaSyB1S9oRPLSjC7XT875sYGTzJXzPOt2QC68",
			"url":    "https://www.googleapis.com/customsearch/v1?cx=017576662512468239146:omuauf_lfve&q=lectures",
		},
		{
			"source": "raja-ongkir",
			"key":    "f7c56fbe569234a113f2518802e44e02",
			"url":    "https://api.rajaongkir.com/starter/province?id=12",
		},
		{
			"source": "open-weather",
			"key":    "ca7a000e6dbb2ea6f11406beab765a67",
			"url":    "http://api.openweathermap.org/data/2.5/find?lat=55.5&lon=37.5&cnt=10",
		},
	}

	posts := []string{
		"https://httpbin.org/post",
		"https://httpbin.org/post",
	}

	var wg sync.WaitGroup
	wg.Add(5)
	for _, api := range apis {

		go func(api map[string]string) {
			defer wg.Done()
			getAPI(api["key"], api["url"], api["source"])
		}(api)

	}

	for _, post := range posts {
		go func(post string) {
			defer wg.Done()
			dataPost(post)
		}(post)
	}

	wg.Wait()
}

func errData() {
	err := recover()
	fmt.Println(err)
}

func getAPI(key, url, source string) {
	defer errData()
	fmt.Println("------------------------------------", source, "is running !!! ------------------------------------")
	var req *http.Request
	var err1 error

	switch source {
	case "raja-ongkir":
		req, err1 = http.NewRequest("GET", url, nil)
		if err1 != nil {
			fmt.Println(err1)
		}

		req.Header.Add("key", key)
	case "open-weather":
		req, err1 = http.NewRequest("GET", url+"&appid="+key, nil)
		if err1 != nil {
			fmt.Println(err1)
		}
	case "google":
		req, err1 = http.NewRequest("GET", url+"&key="+key, nil)
		if err1 != nil {
			fmt.Println(err1)
		}
	}

	res, err2 := http.DefaultClient.Do(req)
	if err2 != nil {
		fmt.Println("error here", source, req.URL)
		fmt.Println(err2)
	}

	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))
}

func dataPost(url string) {
	fmt.Println("------------------------------------", "Post", "------------------------------------")
	requestBody, err1 := json.Marshal(map[string]string{
		"name":  "badak",
		"email": "capbadak@gmail.com",
	})
	if err1 != nil {
		fmt.Println(err1)
	}

	resp, err2 := http.Post(url, "application/json", bytes.NewBuffer(requestBody))
	if err2 != nil {
		fmt.Println(err2)
	}

	body, err3 := ioutil.ReadAll(resp.Body)
	if err3 != nil {
		fmt.Println(err3)
	}
	fmt.Println(string(body))
}
