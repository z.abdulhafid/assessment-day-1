package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var angka = []int{23, 45, 67, 54, 66, 19, 56, 78, 89, 44, 11, 22, 33, 44, 55, 66, 77, 88, 99, 23, 34, 32, 23, 12}

	group := divideToGroup(angka)
	fmt.Println("Group lama adalah:", group)

	newGroup := divideTheGroup(group)
	fmt.Println("Group baru adalah:", newGroup)

	total, min, max, avg := findResult(group)

	group1 := Group{
		Min:   min[0],
		Max:   max[0],
		Total: total[0],
		Avg:   avg[0],
		Arr:   newGroup[0],
	}

	group2 := Group{
		Min:   min[1],
		Max:   max[1],
		Total: total[1],
		Avg:   avg[1],
		Arr:   newGroup[1],
	}

	group3 := Group{
		Min:   min[2],
		Max:   max[2],
		Total: total[2],
		Avg:   avg[2],
		Arr:   newGroup[2],
	}

	maxGroup, minGroup := findMaxMinGroup(group1, group2, group3)

	fmt.Println("Group 1:", group1.getArr())
	fmt.Println("Group 2:", group2.getArr())
	fmt.Println("Group 3:", group3.getArr())

	fmt.Println("Total 1:", group1.getTotal())
	fmt.Println("Total 2:", group2.getTotal())
	fmt.Println("Total 3:", group3.getTotal())

	fmt.Println("Nilai max kumpulan 1", group1.getMax())
	fmt.Println("Nilai min kumpulan 1", group1.getMin())

	fmt.Println("Nilai max kumpulan 2", group2.getMax())
	fmt.Println("Nilai min kumpulan 2", group2.getMin())

	fmt.Println("Nilai max kumpulan 3", group3.getMax())
	fmt.Println("Nilai min kumpulan 3", group3.getMin())

	fmt.Println("Rata-rata 1:", group1.getAvg())
	fmt.Println("Rata-rata 2:", group2.getAvg())
	fmt.Println("Rata-rata 3:", group3.getAvg())

	fmt.Println("Grop terbesar adalah", maxGroup.getArr())
	fmt.Println("Grop terkecil adalah", minGroup.getArr())

}

func errLogs() {
	message := recover()
	if message != nil {
		fmt.Println("Error:", message)
	}
}

func divider(index int, value int, divisor int) (int, error) {
	defer errLogs()

	if divisor == 0 {
		return 100, errors.New("Pembagi di index " + fmt.Sprint(index) + " Adalah 0")
	}
	result := value / divisor
	return result, nil
}

func divideToGroup(angka []int) [3][]int {
	length := len(angka) / 3
	var group [3][]int

	for i := 0; i < 3; i++ {
		group[i] = angka[length*i : length*(i+1)]
	}

	return group
}

func divideTheGroup(group [3][]int) [3][]int {
	rand.Seed(time.Now().UnixNano())
	fmt.Println("Fungsi Pembagi untuk group 1 dijalankan")
	for index, value := range group[0] {
		var err error
		randomNum := rand.Intn(3)
		group[0][index], err = divider(index, value, randomNum)
		if err != nil {
			fmt.Println(err)
		}
	}
	fmt.Println("-----------")
	fmt.Println("Fungsi Pembagi untuk group 2 dijalankan")
	for index, value := range group[1] {
		var err error
		randomNum := rand.Intn(3)
		group[1][index], err = divider(index, value, randomNum)
		if err != nil {
			fmt.Println(err)
		}
	}
	fmt.Println("-----------")
	fmt.Println("Fungsi Pembagi untuk group 3 dijalankan")
	for index, value := range group[2] {
		var err error
		randomNum := rand.Intn(3)
		group[2][index], err = divider(index, value, randomNum)
		if err != nil {
			fmt.Println(err)
		}
	}
	fmt.Println("-----------")

	return group
}

func findResult(group [3][]int) ([3]int, [3]int, [3]int, [3]float32) {
	var total [3]int
	var min [3]int
	var max [3]int
	var avg [3]float32

	min[0] = group[0][0]
	max[0] = group[0][0]
	min[1] = group[1][0]
	max[1] = group[1][0]
	min[2] = group[2][0]
	max[2] = group[2][0]
	for i := 0; i < len(group[0]); i++ {

		//cari min max total group 1
		if max[0] < group[0][i] {
			max[0] = group[0][i]
		}

		if min[0] > group[0][i] {
			min[0] = group[0][i]
		}

		total[0] += group[0][i]

		//cari min max total group 2
		if max[1] < group[1][i] {
			max[1] = group[1][i]
		}

		if min[1] > group[1][i] {
			min[1] = group[1][i]
		}

		total[1] += group[1][i]

		//cari min max total group 3
		if max[2] < group[2][i] {
			max[2] = group[2][i]
		}

		if min[2] > group[2][i] {
			min[2] = group[2][i]
		}

		total[2] += group[2][i]
	}

	avg[0] = float32(total[0] / len(group[0]))
	avg[1] = float32(total[1] / len(group[1]))
	avg[2] = float32(total[2] / len(group[2]))

	return total, min, max, avg
}

func findMaxMinGroup(group1 ThisGroup, group2 ThisGroup, group3 ThisGroup) (ThisGroup, ThisGroup) {
	group := [3]ThisGroup{group1, group2, group3}

	var maxGroup int = 0
	var minGroup int = 0
	for i := 0; i < 2; i++ {
		if group[i].getMax() > group[i+1].getMax() {
			maxGroup = i
		}

		if group[i].getMin() < group[i+1].getMin() {
			minGroup = i
		}
	}

	return group[maxGroup], group[minGroup]
}

// Group comment.
type Group struct {
	Min, Max, Total int
	Avg             float32
	Arr             []int
}

// ThisGroup comment.
type ThisGroup interface {
	getMin() int
	getMax() int
	getTotal() int
	getAvg() float32
	getArr() []int
}

func (group Group) getMin() int {
	return group.Min
}

func (group Group) getMax() int {
	return group.Max
}

func (group Group) getTotal() int {
	return group.Total
}

func (group Group) getAvg() float32 {
	return group.Avg
}

func (group Group) getArr() []int {
	return group.Arr
}
