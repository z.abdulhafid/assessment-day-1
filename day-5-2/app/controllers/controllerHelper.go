package controllers

import (
	"assesmentbulk/app/models"
	"assesmentbulk/app/syshelper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// UserOnOffice function
func (strDB *StrDB) UserOnOffice(c *gin.Context) {
	var (
		officeuser []models.OfficeUser
		office     []models.Office
		result     gin.H
		err        syshelper.ErrorMessage
	)

	defer err.SystemErrorHandler()

	msg := strDB.DB.Model(&office).Select("offices.name, offices.address, users.fullname, users.email").Joins("inner join users on users.office_id = offices.id").Scan(&officeuser).Error
	if msg != nil {
		err.CustomErrorHandler(msg)
	}

	if len(err.StringError) <= 0 {
		result = syshelper.ResponseSuccess(&officeuser)
	} else {
		result = syshelper.ResponseFailed(err.StringError)
	}

	c.JSON(http.StatusOK, result)
}

// UserJobs function
func (strDB *StrDB) UserJobs(c *gin.Context) {
	var (
		usertodos []models.UserTodos
		user      []models.User
		result    gin.H
		err       syshelper.ErrorMessage
		msg       error
	)

	defer err.SystemErrorHandler()

	id := c.Param("id")

	if id == "" {
		msg = strDB.DB.Model(&user).Select("users.fullname, users.email, todos.name, todos.description").Joins("inner join todos on todos.user_id = users.id").Scan(&usertodos).Error
	} else {
		msg = strDB.DB.Model(&user).Select("users.fullname, users.email, todos.name, todos.description").Joins("inner join todos on todos.user_id = users.id").Where("users.id = ?", id).Scan(&usertodos).Error
	}

	if msg != nil {
		err.CustomErrorHandler(msg)
	}

	if len(err.StringError) <= 0 {
		result = syshelper.ResponseSuccess(&usertodos)
	} else {
		result = syshelper.ResponseFailed(err.StringError)
	}

	c.JSON(http.StatusOK, result)
}

// OfficeJobs function
func (strDB *StrDB) OfficeJobs(c *gin.Context) {
	var (
		officetodos []models.OfficeTodos
		office      []models.Office
		result      gin.H
		err         syshelper.ErrorMessage
		msg         error
	)

	defer err.SystemErrorHandler()

	id := c.Param("id")

	if id == "" {
		msg = strDB.DB.Model(&office).Select("offices.name as name, offices.address as address, todos.name as jobs, todos.description as description").Joins("inner join users on users.office_id = offices.id").Joins("inner join todos on todos.user_id = users.id").Scan(&officetodos).Error
	} else {
		msg = strDB.DB.Model(&office).Select("offices.name as name, offices.address as address, todos.name as jobs, todos.description as description").Joins("inner join users on users.office_id = offices.id").Joins("inner join todos on todos.user_id = users.id").Where("offices.id = ?", id).Scan(&officetodos).Error
	}

	if msg != nil {
		err.CustomErrorHandler(msg)
	}

	if len(err.StringError) <= 0 {
		result = syshelper.ResponseSuccess(&officetodos)
	} else {
		result = syshelper.ResponseFailed(err.StringError)
	}

	c.JSON(http.StatusOK, result)
}

// OfficeUserJobs function
func (strDB *StrDB) OfficeUserJobs(c *gin.Context) {
	var (
		officetodos []models.OfficeTodos
		office      []models.Office
		result      gin.H
		err         syshelper.ErrorMessage
		msg         error
	)

	defer err.SystemErrorHandler()

	officeid := c.Query("officeid")
	userid := c.Query("userid")

	msg = strDB.DB.Model(&office).Select("offices.name as name, offices.address as address, todos.name as jobs, todos.description as description").Joins("inner join users on users.office_id = offices.id").Joins("inner join todos on todos.user_id = users.id").Where("offices.id = ? or users.id = ?", officeid, userid).Scan(&officetodos).Error
	if msg != nil {
		err.CustomErrorHandler(msg)
	}

	if len(err.StringError) <= 0 {
		result = syshelper.ResponseSuccess(&officetodos)
	} else {
		result = syshelper.ResponseFailed(err.StringError)
	}

	c.JSON(http.StatusOK, result)
}

// OfficeByUser function
func (strDB *StrDB) OfficeByUser(c *gin.Context) {
	var (
		office models.Office
		result gin.H
		err    syshelper.ErrorMessage
	)

	defer err.SystemErrorHandler()

	id := c.Param("id")
	msg := strDB.DB.Joins("join users on users.office_id = offices.id").Where("users.id = ?", id).Find(&office).Error
	if msg != nil {
		err.CustomErrorHandler(msg)
	}

	if len(err.StringError) <= 0 {
		result = syshelper.ResponseSuccess(&office)
	} else {
		result = syshelper.ResponseFailed(err.StringError)
	}

	c.JSON(http.StatusOK, result)
}

// UserByJob function
func (strDB *StrDB) UserByJob(c *gin.Context) {
	var (
		user   models.User
		result gin.H
		err    syshelper.ErrorMessage
	)

	defer err.SystemErrorHandler()

	id := c.Param("id")
	msg := strDB.DB.Joins("join todos on todos.user_id = users.id").Where("todos.id = ?", id).Find(&user).Error
	if msg != nil {
		err.CustomErrorHandler(msg)
	}

	if len(err.StringError) <= 0 {
		result = syshelper.ResponseSuccess(&user)
	} else {
		result = syshelper.ResponseFailed(err.StringError)
	}

	c.JSON(http.StatusOK, result)
}

// OfficeByJob function
func (strDB *StrDB) OfficeByJob(c *gin.Context) {
	var (
		office models.Office
		result gin.H
		err    syshelper.ErrorMessage
	)

	defer err.SystemErrorHandler()

	id := c.Param("id")
	msg := strDB.DB.Joins("join users on users.office_id = offices.id").Joins("join todos on todos.user_id = users.id").Where("todos.id = ?", id).Find(&office).Error
	if msg != nil {
		err.CustomErrorHandler(msg)
	}

	if len(err.StringError) <= 0 {
		result = syshelper.ResponseSuccess(&office)
	} else {
		result = syshelper.ResponseFailed(err.StringError)
	}

	c.JSON(http.StatusOK, result)
}
