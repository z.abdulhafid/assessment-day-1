package syshelper

import "github.com/gin-gonic/gin"

// ResponseSuccess function
func ResponseSuccess(data interface{}) gin.H {
	return gin.H{
		"status": "Success",
		"data":   data,
	}
}

// ResponseFailed function
func ResponseFailed(strError []error) gin.H {
	var errs []string

	for _, str := range strError {
		errs = append(errs, str.Error())
	}

	return gin.H{
		"status":  "Errors",
		"message": errs,
	}
}
